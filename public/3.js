const f = require('fs');
let data = f.readFileSync('control.txt'); // Считываем файл.
data = data.toString(); // Переводим в string.
data = data.split(/<[^>]+>/g); // Убираем все теги.
let text = "";
// Итерируем array.
data.forEach(function (piece) {
    // Вырезаем пробелы с кажого куска.
    piece = piece.trim();
    // Обьединяем все текста в один общий текст.
    if (piece.length) {
        text = text + ' ' + piece;
    }
});
// String обязательных букв по которым идет проверка.
const requiredChars = 'abcdefghijklmnopqrstuvwxyz' + 'абвгдеёжзийклмнопрстуфхцчшщъыьэюя';
const duplicatedTwoChar = []; // Все повторяющиеся двухбуквенные слова.
const uniqueTwoChar = []; // Уникальные двухбуквенные слова.
// Разбиваем общий текст через пробелы.
text = text.split(' ');
// Итерируем array.
text.forEach(function(piece) {
    // Если слово (стринг) длинною в два символа и при этом первый и второй символ есть в string-е обязательных букв.
    if (piece.length === 2 && requiredChars.indexOf(piece[0]) !== -1 && requiredChars.indexOf(piece[1]) !== -1) {
        // Добавляем слово в список повторяющихся слов.
        duplicatedTwoChar.push(piece);
        // Если слово отсутствует в списке уникальных слов добавляем, если нет то пропускаем.
        if (uniqueTwoChar.indexOf(piece) === -1) {
            uniqueTwoChar.push(piece);
        }
    }
});
console.log("Все двухбуквенные слова", duplicatedTwoChar);
console.log("Уникальные двухбуквенные слова", uniqueTwoChar);
console.log("Количество всех двухбуквенных слов", duplicatedTwoChar.length);
console.log("Количество уникальных двухбуквенных слов", uniqueTwoChar.length);